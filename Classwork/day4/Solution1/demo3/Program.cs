﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03OOPDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Department department = new Department();
            //department.DeptNo = 100;
            //department.DeptName = "Admin";

            Employee employee = new Employee();
            employee.No = 1;
            employee.Name = "Mahesh";
            //employee.EDepartment = department;

            employee.PrintDetails();
            Console.ReadLine();
        }
    }

    public class Employee
    {
        private int _No;
        private string _Name;
        private Department _EDepartment;

        public Department EDepartment
        {
            get { return _EDepartment; }
            set { _EDepartment = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

        public void PrintDetails()
        {
            Console.WriteLine(this.No.ToString() + "   " + this.Name + "  " + this.EDepartment.DeptNo + this.EDepartment.DeptName);
        }
    }

    public class Department
    {
        private int _DeptNo;
        private string _DeptName;

        private Address address;

        public Address DeptAddress
        {
            get { return address; }
            set { address = value; }
        }

        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }

        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }

    }

    public class Address
    {
        private string _City;
        private string _State;

        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        public string City
        {
            get { return _City; }
            set { _City = value; }
        }

    }
}
