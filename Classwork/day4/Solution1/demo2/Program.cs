﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02OOPDemo
{
    //Demo
    class Program
    {
        static void Main(string[] args)
        {
            // ReportFactory rf = new ReportFactory();

            Console.WriteLine("1: PDF, 2: DOCX 3: PPT 4: Excel 5: Text");
            int choice = Convert.ToInt32(Console.ReadLine());

            //Report report = rf.GetReport(choice); //new DOCX() or new PDF()
            Report report = ReportFactory.GetReport(choice); //new DOCX() or new PDF()

            //Third Developer using Report Objects for Printing Purose.......
            report.Execute();

            Console.ReadLine();
        }
    }
    public class ReportFactory
    {
        public static Report GetReport(int choice)
        {
            if (choice == 1)
            {
                return new PDF();
            }

            else if (choice == 2)
            {
                return new DOCX();
            }
            else if (choice == 3)
            {
                return new PPT();
            }
            else if (choice == 4)
            {
                return new Excel();
            }
            else
            {
                return new Text();
            }

        }
    }      //Architect
    public abstract class Report
    {
        protected abstract void Read();
        protected abstract void Parse();
        protected abstract void Validate();
        protected abstract void Print();

        public virtual void Execute()
        {
            Read();
            Parse();
            Validate();
            Print();
        }
    }   //Architect
    public abstract class SpecialReport : Report
    {
        protected abstract void ReValidate();
        public override void Execute()
        {
            Read();
            Parse();
            Validate();
            ReValidate();
            Print();
        }
    }  //Architect
    public class PDF : Report
    {
        protected override void Read()
        {
            //Step 1
            Console.WriteLine("Read PDF");
        }
        protected override void Parse()
        {
            //Step 2
            Console.WriteLine("Parsing PDF");
        }
        protected override void Validate()
        {
            //Step 3
            Console.WriteLine("Validating PDF");
        }
        protected override void Print()
        {
            //Step 4
            Console.WriteLine("PDF Getting Printed Here...");
        }
    }               //Written By Developer 1
    public class DOCX : Report
    {
        protected override void Read()
        {
            //Step 1
            Console.WriteLine("Read DOCX");
        }
        protected override void Parse()
        {
            //Step 2
            Console.WriteLine("Parsing DOCX");
        }
        protected override void Validate()
        {
            //Step 3
            Console.WriteLine("Validating DOCX");
        }
        protected override void Print()
        {
            //Step 4
            Console.WriteLine("DOCX Getting Printed Here...");
        }
    }             //Written By Developer 2
    public class PPT : Report
    {
        protected override void Parse()
        {
            Console.WriteLine("PPT Parsed");
        }
        protected override void Print()
        {
            Console.WriteLine("PPT Printed");
        }

        protected override void Read()
        {
            Console.WriteLine("PPT Read");
        }

        protected override void Validate()
        {
            Console.WriteLine("PPT Validated");
        }
    }              //Written By Developer 4
    public class Excel : SpecialReport
    {
        protected override void Parse()
        {
            Console.WriteLine("Excel Parsed");
        }

        protected override void Print()
        {
            Console.WriteLine("Excel Printed");
        }

        protected override void Read()
        {
            Console.WriteLine("Excel Read");
        }

        protected override void Validate()
        {
            Console.WriteLine("Excel Validated");
        }

        protected override void ReValidate()
        {
            Console.WriteLine("Excel ReValidated");
        }


    }            //Written By Developer 5 --- Addition of one more method and sequence change
    public class Text : SpecialReport
    {
        protected override void Parse()
        {
            Console.WriteLine("TXT Parsed");
        }

        protected override void Print()
        {
            Console.WriteLine("TXT Printed");
        }

        protected override void Read()
        {
            Console.WriteLine("TXT Read");
        }

        protected override void Validate()
        {
            Console.WriteLine("TXT Validated");
        }

        protected override void ReValidate()
        {
            Console.WriteLine("TXT ReValidated");
        }


    }             //Written By Developer 6
}
