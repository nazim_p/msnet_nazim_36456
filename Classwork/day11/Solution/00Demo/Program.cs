﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;


namespace _00Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"G:\msnet_nazim_36456\Classwork\day11\Solution\MathsLib\bin\Debug\MathsLib.dll";
            Assembly assemblyInfo = Assembly.LoadFrom(path);
            Type[] alltypes = assemblyInfo.GetTypes();
            foreach (Type type in alltypes)
            {
                Console.WriteLine(type.Name);
                MethodInfo[] allMethods = type.GetMethods();
                foreach (MethodInfo method in allMethods)
                {
                    Console.WriteLine("--------" + method.Name);
                }
                //  MethodInfo[] allMethods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                foreach (MethodInfo method in allMethods)
                {
                    Console.Write("--- {0}     {1} ( ", method.ReturnType.ToString(), method.Name);

                    ParameterInfo[] allParameters = method.GetParameters();
                    foreach (ParameterInfo parameter in allParameters)
                    {
                        Console.Write(" {0} {1} ", parameter.ParameterType.ToString(), parameter.Name);
                    }

                    Console.Write(" ) ");
                    Console.WriteLine();

                }
            }
        }
    }
}
