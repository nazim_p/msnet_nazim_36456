﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00demo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Dynamic Type
            //Console.WriteLine("Enter the choice");
            //int choice=Convert.ToInt32(Console.ReadLine());
            ////Object obj = GetMeSomeObject(choice);
            //dynamic obj = GetMeSomeObject(choice);
            //Console.WriteLine(obj.getDetails());
            ////if(obj is Emp)
            ////{
            ////    Emp emp = (Emp)obj;
            ////    Console.WriteLine(emp.getDetails());
            ////}
            ////else if(obj is Book)
            ////{
            ////    Book book = (Book)obj;
            ////    Console.WriteLine(book.getBookDetails());
            ////}
            #endregion
            
        //    Emp e = new Emp();
        //    string data = e.getDetails(1, name: "nazim");
        //    Console.WriteLine(data);

        //}
        //static object GetMeSomeObject(int choice)
        //{
        //    if (choice == 1)
        //    {
        //        Emp emp = new Emp();
        //        return emp;
        //    }
        //    else
        //    {
        //        Book book = new Book();
        //        return book;
        //    }
        //}
        
    }

        public class Emp
    {
        public string getDetails(int no,string name="Gopal",string address="Jalna")
        {
            return string.Format("no={0},name={1},address={2}", no, name, address);
        }
        //public string getDetails()
        //{
        //    return "Some EMP Details";
        //}
    }
    public class Book
    {
        public string getBookDetails()
        {
            return "Some BOOK Details";
        }
    }
}
