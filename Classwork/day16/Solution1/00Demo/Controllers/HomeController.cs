﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _00Demo.Models;
using _00Demo.Filters;
using System.Data.SqlClient;
namespace _00Demo.Controllers
{
    public class HomeController :BaseController
    {
        // GET: Home
        Karad dbObj = new Karad();
      // [Authrize]
           
        public ActionResult Index()
        {
            var emps = dbObj.Emps.ToList();
            return View(emps);
        }
        public ActionResult Edit(int id)
        {
            var empToBeUpdated = (from emp in dbObj.Emps
                                  where emp.No == id
                                  select emp).First();
            return View(empToBeUpdated);
        }
        public ActionResult AfterEdit(FormCollection entireFormData)
        {
            int id = Convert.ToInt32(entireFormData["No"]);
            string latestname = entireFormData["Name"].ToString();
            string latestAddress = entireFormData["Address"].ToString();
            var empToBeUpdated = (from emp in dbObj.Emps
                                  where emp.No == id
                                  select emp).First();

            empToBeUpdated.Name = latestname;
            empToBeUpdated.Address = latestAddress;

            dbObj.SaveChanges();

            return Redirect("/Home/Index");

        }
        public ActionResult Delete(int id)
        {
            var empToBeDeleted = (from emp in dbObj.Emps
                                  where emp.No == id
                                  select emp).First();
            dbObj.Emps.Remove(empToBeDeleted);
            dbObj.SaveChanges();

            return Redirect("/Home/Index");
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult AfterCreate(FormCollection entireFormData)
        {
            int id = Convert.ToInt32(entireFormData["No"]);
            string name = entireFormData["Name"].ToString();
            string address = entireFormData["Address"].ToString();
            Emp emp = new Emp() { No = id, Name = name, Address = address };
            dbObj.Emps.Add(emp);
            dbObj.SaveChanges();

            return Redirect("/Home/Index");

        }
    }
}