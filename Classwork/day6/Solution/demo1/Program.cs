﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace _01DemoOOP
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Boxing UnBoxing
            //int i = 100;
            //short s = 20;
            //Person p = new Person();
            //p.Name = Console.ReadLine();

            //int i = 100;     //Stack              //Over Stack - Value TYpe - Fixed Size
            //object obj = i;  //Boxing             //Object is reference Type , Size can vary
            ////obj = "abc";
            ////obj = true;

            ////obj = 10.2;
            ////obj = new Person();

            //int j = Convert.ToInt32(obj);  //UnBoxing

            //string s = "abc"; //ref type , heap         //=== String s = new String(new char[]{'a', 'b', 'c'});

            //object obj1 = s;  //heap -> heap  ..casting
            //string s2 = Convert.ToString(obj1); // casting
            #endregion

            #region Simple Casting
            //object obj = new object();
            //Person p = new Person();
            //obj = p;

            //Person p1 =(Person) obj;
            // Person p1 = obj as Person;
            #endregion

            #region Int Array
            //int[] arr = new int[3];

            //arr[0] = 100;
            //arr[1] = 200;
            //arr[2] = 300;
            //arr[3] = 199;  //This will raise an exception

            //for (int i = 0; i < arr.Length; i++)
            //{
            //    Console.WriteLine(arr[i].ToString());
            //}
            #endregion

            Employee e1 = new Employee(1, "mahesh", "pune");
            Employee e2 = new Employee(2, "shubham", "karad");
            Employee e3 = new Employee(3, "rahul", "kolhapur");
            Employee e4 = new Employee(4, "akash", "mumbai");

            #region Employee Array
            //Employee[] emps = new Employee[] { e1, e2,e3,e4 };
            //Console.WriteLine(emps.Length);

            //Employee[] emps = new Employee[4];
            //emps[0] = e1;
            //emps[1] = e2;
            //emps[2] = e3;
            //emps[3] = e4;

            //for (int i = 0; i < emps.Length; i++)
            //{
            //    Console.WriteLine(emps[i].Name);
            //}

            //foreach (Employee e in emps)
            //{
            //    Console.WriteLine(e.Name);
            //}

            #endregion

            #region Object Array
            //object[] arr = new object[] { "abcd", 100, e1, 200, e2, e3, true };
            //foreach (object obj in arr)
            //{
            //    if (obj is int)
            //    {
            //        int j = Convert.ToInt32(obj); //Unboxing
            //        Console.WriteLine(j);
            //    }
            //    else if(obj is string)
            //    {

            //        string s = Convert.ToString(obj);  //Casting
            //        //s = "pqrs";
            //        Console.WriteLine(s);
            //    }
            //    else if(obj is Employee)
            //    {
            //        Employee e = (Employee)obj;        //Casting
            //        Console.WriteLine(e.Name);
            //    }
            //    else
            //    {
            //        Console.WriteLine("UnKnown Data!");
            //    }
            //}
            #endregion

            #region ArrayList
            //ArrayList arr = new ArrayList();
            //arr.Add("abcd");
            //arr.Add(100);
            //arr.Add(e1);
            //arr.Add(200);
            //arr.Add(e2);
            //arr.Add(e3);
            //arr.Add(true);

            ////object obj1 = arr[3];
            ////if (obj1 is int)
            ////{
            ////    Console.WriteLine(obj1.ToString());
            ////}

            //foreach (object obj in arr)
            //{
            //    if (obj is int)
            //    {
            //        int j = Convert.ToInt32(obj); //Unboxing
            //        Console.WriteLine(j);
            //    }
            //    else if (obj is string)
            //    {

            //        string s = Convert.ToString(obj);  //Casting
            //        //s = "pqrs";
            //        Console.WriteLine(s);
            //    }
            //    else if (obj is Employee)
            //    {
            //        Employee e = (Employee)obj;        //Casting
            //        Console.WriteLine(e.Name);
            //    }
            //    else
            //    {
            //        Console.WriteLine("UnKnown Data!");
            //    }
            //}
            #endregion

            #region HashTable
            //    Hashtable arr = new Hashtable();
            //    arr.Add("a", "abcd");
            //    arr.Add("b", 100);
            //    arr.Add("c", e1);
            //    arr.Add("d", 200);
            //    arr.Add("e", e2);
            //    arr.Add("f", e3);

            //    Console.WriteLine("What would you like to search in collection?");
            //    string keyToBeSearched = Console.ReadLine();
            //    object obj = arr[keyToBeSearched];
            //    if (obj !=null)
            //    {
            //        if (obj is int)
            //        {
            //            int j = Convert.ToInt32(obj); //Unboxing
            //            Console.WriteLine(j);
            //        }
            //        else if (obj is string)
            //        {

            //            string s = Convert.ToString(obj);  //Casting
            //                                               //s = "pqrs";
            //            Console.WriteLine(s);
            //        }
            //        else if (obj is Employee)
            //        {
            //            Employee e = (Employee)obj;        //Casting
            //            Console.WriteLine(e.Name);
            //        }
            //        else
            //        {
            //            Console.WriteLine("UnKnown Data!");
            //        }
            //    }
            //    else
            //    {
            //        Console.WriteLine("Specified Key Does not exist!!");
            //    }
            //}
            #endregion

            #region HashTable Again

            //Hashtable arr = new Hashtable();
            //arr.Add("a", "abcd");
            //arr.Add("b", 100);
            //arr.Add("c", e1);
            //arr.Add("d", 200);
            //arr.Add("e", e2);
            //arr.Add("f", e3);

            //foreach (object key in arr.Keys)
            //{
            //    Console.WriteLine("Key is : " + key.ToString());

            //    object obj = arr[key];

            //    if (obj is int)
            //    {
            //        int j = Convert.ToInt32(obj); //Unboxing
            //        Console.WriteLine(j);
            //    }
            //    else if (obj is string)
            //    {

            //        string s = Convert.ToString(obj);  //Casting
            //                                            //s = "pqrs";
            //        Console.WriteLine(s);
            //    }
            //    else if (obj is Employee)
            //    {
            //        Employee e = (Employee)obj;        //Casting
            //        Console.WriteLine(e.Name);
            //    }
            //    else
            //    {
            //        Console.WriteLine("UnKnown Data!");
            //    }
            //}
            #endregion
        }
    }

    public class Employee
    {
        private int _No;
        private string _Name;
        private string _Address;

        public Employee(int no, string name, string address)
        {
            this.No = no;
            this.Name = name;
            this.Address = address;
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int No
        {
            get { return _No; }
            set { _No = value; }
        }

    }
}
