﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace _01Demo
{
	class Program
	{
		static void Main(string[] args)
		{

			string connectionString = ConfigurationManager.ConnectionStrings["dbconnection"].ToString();

			int choice = Convert.ToInt32(ConfigurationManager.AppSettings["dbCategory"]);

			DBFactory dbf = new DBFactory();

			IDatabase obj = dbf.GetDBObject(choice, connectionString);

			var emps = obj.Select();
			foreach (var emp in emps)
			{
				Console.WriteLine(emp.Name);
			}

		}
	}

	public interface IDatabase
	{
		List<Emp> Select();
		int Insert(Emp emp);
		int Update(Emp emp);
		int Delete(int id);
	}

	public class Oracle : IDatabase
	{
		private string connectionString = "";

		public Oracle(string connectionstring)
		{
			this.connectionString = connectionstring;
		}
		public List<Emp> Select()
		{
			List<Emp> emps = new List<Emp>();
			//As if you connected to Oracle DB and fetched some data
			emps.Add(new Emp() { No = 100, Name = "John", Address = "NY" });
			emps.Add(new Emp() { No = 200, Name = "Walter", Address = "Ohio" });
			emps.Add(new Emp() { No = 300, Name = "Scott", Address = "Texas" });
			return emps;
		}
		public int Insert(Emp emp)
		{
			return 1;
		}
		public int Update(Emp emp)
		{
			return 1;
		}
		public int Delete(int id)
		{
			return 1;
		}
	}
	public class SQLServer : IDatabase
	{
		private string connectionString = "";
		public SQLServer(string connectionstring)
		{
			this.connectionString = connectionstring;
		}
		public List<Emp> Select()
		{
			List<Emp> emps = new List<Emp>();

			SqlConnection connection = new SqlConnection(connectionString);
			connection.Open();
			SqlCommand command = new SqlCommand("select * from Emp", connection);
			SqlDataReader reader = command.ExecuteReader();

			while (reader.Read())
			{
				Emp emp = new Emp();
				emp.No = Convert.ToInt32(reader["No"]);
				emp.Name = reader["Name"].ToString();
				emp.Address = reader["Address"].ToString();

				emps.Add(emp);
			}

			connection.Close();
			return emps;
		}
		public int Insert(Emp emp)
		{
			string queryFormat = "insert into Emp values({0},'{1}', '{2}')";
			string finalQuery = string.Format(queryFormat, emp.No, emp.Name, emp.Address);

			SqlConnection connection = new SqlConnection(connectionString);
			connection.Open();
			SqlCommand command = new SqlCommand(finalQuery, connection);
			int RowsAffected = command.ExecuteNonQuery();
			connection.Close();

			return RowsAffected;
		}
		public int Update(Emp emp)
		{
			string queryFormat = "update Emp set Name='{1}', Address='{2}' where No = {0}";
			string finalQuery = string.Format(queryFormat, emp.No, emp.Name, emp.Address);

			SqlConnection connection = new SqlConnection(connectionString);
			connection.Open();
			SqlCommand command = new SqlCommand(finalQuery, connection);
			int RowsAffected = command.ExecuteNonQuery();
			connection.Close();
			return RowsAffected;
		}
		public int Delete(int id)
		{
			string queryFormat = "Delete from Emp where No = {0}";
			string finalQuery = string.Format(queryFormat, id);

			SqlConnection connection = new SqlConnection(connectionString);
			connection.Open();
			SqlCommand command = new SqlCommand(finalQuery, connection);
			int RowsAffected = command.ExecuteNonQuery();
			connection.Close();
			return RowsAffected;
		}
	}

	public class DBFactory
	{
		public IDatabase GetDBObject(int choice, string connectionString)
		{
			if (choice == 1)
			{
				return new SQLServer(connectionString);
			}
			else
			{
				return new Oracle(connectionString);
			}
		}
	}
	public class Emp
	{
		public int No { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
	}
}
