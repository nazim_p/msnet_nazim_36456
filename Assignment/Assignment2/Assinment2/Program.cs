﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyConsoleApplication
{
    public struct student
    {
        private String _name;
        private string _gender;
        private int _age;
        private int _std;
        private char _div;
        private double _marks;

        public student(string name, string gender, int age, int std, char div, double marks)
        {
            this._name = name;
            this._gender = gender;
            this._age = age;
            this._std = std;
            this._div = div;
            this._marks = marks;
        }
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public string gender
        {
            get
            {
                return _gender;
            }
            set
            {
                _gender = value;
            }
        }

        public int age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }


        public int std
        {
            get
            {
                return _std;
            }
            set
            {
                _std = value;
            }
        }

        public char div
        {
            get
            {
                return _div;
            }
            set
            {
                _div = value;
            }
        }

        public double marks
        {
            get
            {
                return _marks;
            }
            set
            {
                _marks = value;
            }
        }

        public void AcceptDetails()
        {
            Console.WriteLine("Enter the Name:  ");
            string Aname = Console.ReadLine();
            name = Aname;
            Console.WriteLine("Enter the Gender:    ");
            string Gender = Console.ReadLine();
            gender = Gender;
            Console.Write("Enter the Age:   ");
            String str1 = Console.ReadLine();
            int Age = Convert.ToInt32(str1);
            age = Age;
            Console.Write("Enter the Std:   ");
            String str2 = Console.ReadLine();
            int Std = Convert.ToInt32(str2);
            std = Std;
            Console.WriteLine("Enter The Divison:   ");
            string str3 = Console.ReadLine();
            char Div = Convert.ToChar(str3);
            div = Div;
            Console.WriteLine("Enter the Marks: ");
            string str4 = Console.ReadLine();
            int Marks = Convert.ToInt32(str4);
            marks = Marks;
        }

        public void PrintDetails()
        {
            Console.WriteLine("Name:        " + name);
            Console.WriteLine("Gender:      " + gender);
            Console.WriteLine("Age:         " + age);
            Console.WriteLine("Std:         " + std);
            Console.WriteLine("Div:         " + div);
            Console.WriteLine("Marks:       " + marks);
            Console.ReadLine();
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            student s = new student();
            //student s = new student("shubham","male",24,10,'c',100);
            s.AcceptDetails();
            // s.name = "ABC";
            s.PrintDetails();
        }
    }
}