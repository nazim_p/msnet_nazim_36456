﻿using HR_Assign.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HR_Assign.Controllers
{
    public class EmployeeController : Controller
    {
        Hrdb dbObj = new Hrdb();
        public ActionResult Index()
        {
            var emps = dbObj.Employees.ToList();
            return View(emps);
        }
        public ActionResult Show()
        {

            Hrdb dbObj = new Hrdb();
            var emps = dbObj.Employees.ToList();
            return View("ListEmployee", emps);
        }
        public ActionResult UpdateEmployee(int id, string FName, string email, string pass, float sal,
                                            float com, string des, string mob)
        {
            var empToBeUpdated = (from emp in dbObj.Employees
                                  where emp.EmpNo == id
                                  select emp).First();
            return View(empToBeUpdated);
        }

        public ActionResult AfterUpdate(Employee empUpdated)
        {
            var empToBeUpdated = (from emp in dbObj.Employees
                                  where emp.EmpNo == empUpdated.EmpNo
                                  select emp).First();
            empToBeUpdated.Name = empUpdated.Name;
            empToBeUpdated.Email = empUpdated.Email;
            empToBeUpdated.Designation = empUpdated.Designation;
            empToBeUpdated.Salary = empUpdated.Salary;
            empToBeUpdated.Passcode = empUpdated.Passcode;
            empToBeUpdated.Mobileno = empUpdated.Mobileno;
            empToBeUpdated.Commission = empUpdated.Commission;

            dbObj.SaveChanges();

            return Redirect("/Employee/Index");
        }


        public ActionResult DeleteEmployee(int id)
        {
            var empToBeDeleted = (from emp in dbObj.Employees
                                  where emp.EmpNo == id
                                  select emp).First();
            dbObj.Employees.Remove(empToBeDeleted);
            dbObj.SaveChanges();
            return Redirect("/Employee/Index");
        }

        // [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CreateEmployee()
        {
            return View();
        }

        //[HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateEmployee(Employee emp)
        {
            dbObj.Employees.Add(emp);
            /* dbObj.SaveChanges();*/
            return Redirect("/Employee/Index");
        }


        public ActionResult AfterCreate(Employee emp)
        {
            dbObj.Employees.Add(emp);
            dbObj.SaveChanges();
            return Redirect("/Employee/Index");
        }


    }
}


