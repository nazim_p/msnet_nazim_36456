﻿using HR_Assign.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HR_Assign.Controllers
{
    public class DeptController : Controller
    {
        Hrdb dbObj = new Hrdb();
        public ActionResult Index()
        {
            var depts = dbObj.Departments.ToList();
            return View(depts);
        }

        public ActionResult Show()
        {
            Hrdb dbObj = new Hrdb();
            var depts = dbObj.Departments.ToList();
            return View("ListDept", depts);
            //return Redirect("/Dept/ListDept");

        }
        public ActionResult UpdateDept(int id, string FName, string ECity)
        {
            var deptToBeUpdated = (from dept in dbObj.Departments
                                   where dept.DeptNo == id
                                   select dept).First();
            return View(deptToBeUpdated);
        }

        public ActionResult AfterUpdate(FormCollection entireFormData)
        {
            int id = Convert.ToInt32(entireFormData["DeptNo"]);
            string latestname = entireFormData["DeptName"].ToString();
            string latestaddress = entireFormData["Location"].ToString();

            var deptToBeUpdated = (from dept in dbObj.Departments
                                   where dept.DeptNo == id
                                   select dept).First();
            deptToBeUpdated.DeptName = latestname;
            deptToBeUpdated.Location = latestaddress;

            dbObj.SaveChanges();

            return Redirect("/Dept/Index");
        }


        public ActionResult DeleteDept(int id)
        {
            var deptToBeDeleted = (from dept in dbObj.Departments
                                   where dept.DeptNo == id
                                   select dept).First();
            dbObj.Departments.Remove(deptToBeDeleted);
            dbObj.SaveChanges();
            return Redirect("/Dept/Index");
        }


        public ActionResult CreateDept()
        {
            return View();
        }

        public ActionResult AfterCreate(FormCollection entireFormData)
        {
            int id = Convert.ToInt32(entireFormData["DeptNo"]);
            string name = entireFormData["DeptName"].ToString();
            string address = entireFormData["Location"].ToString();

            Department dept = new Department() { DeptNo = id, DeptName = name, Location = address };
            dbObj.Departments.Add(dept);

            dbObj.SaveChanges();

            return Redirect("/Dept/Index");
        }

    }
}