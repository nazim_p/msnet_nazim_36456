//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace demo.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Employee
    {
        public Employee()
        {
            this.Employee1 = new HashSet<Employee>();
        }
    
        public int EmpNo { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Passcode { get; set; }
        public Nullable<double> Salary { get; set; }
        public Nullable<double> Commission { get; set; }
        public string Designation { get; set; }
        public Nullable<System.DateTime> Hiredate { get; set; }
        public Nullable<int> Mobileno { get; set; }
        public Nullable<int> DeptNo { get; set; }
    
        public virtual ICollection<Employee> Employee1 { get; set; }
        public virtual Employee Employee2 { get; set; }
    }
}
