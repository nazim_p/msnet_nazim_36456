﻿using HR_Assign.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace HR_Assign.Controllers
{
    public class UserController : Controller
    {
        HrdbEntities obj = new HrdbEntities();
        // GET: User
        public ActionResult SignIn()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignIn(FormCollection userdata, string returnUrl)
        {
            string UserName = userdata["UserName"].ToString();
            string Password = userdata["Password"].ToString();
            var emps = obj.Employees.ToList();
            var query = (from e in emps where e.Email == UserName && e.Passcode == Password select e).FirstOrDefault();
            if (query != null)
            {
                FormsAuthentication.SetAuthCookie(UserName, false);

                if (returnUrl != null)
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return Redirect("/Employee/Index");
                }
            }
            else
            {
                ViewBag.ErrorMessage = "UserName/Password is Incorrect";
                //ViewData["Errmsg"] = "UserName/Password is Incorrect";
                return View();
            }
        }
    }
}