﻿using HR_Assign.Filters;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HR_Assign.Controllers
{
    [LogFilter]
    [Authorize]
    [HandleError(ExceptionType = typeof(SqlException), View = "error")]
    /*[HandleError(ExceptionType = typeof(DivideByZeroException), View = "Error2")]
    [HandleError(ExceptionType = typeof(OleDbException), View = "Error3")]*/
    //[HandleError(ExceptionType = typeof(Exception), View = "Error")]
    public class BaseController : Controller
    {
        //HandleErrorInfo
    }
}
