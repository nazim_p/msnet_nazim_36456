﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
namespace Assignment3
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.GetLog.log("Main method Executed");
            Product p = new Product();
            p.WriteDetails();
            p.ReadDetails();
            Console.ReadLine();

        }

        [Serializable]
        public class Product
        {
            private string _title;
            private double _price;
            private string _Category;
            private string _Manufacturer;
            [NonSerialized]
            private string _ProductCode;

            public string ProductCode
            {
                get { return _ProductCode; }
                set { _ProductCode = value; }
            }

            public string Manufacturer
            {
                get { return _Manufacturer; }
                set { _Manufacturer = value; }
            }


            public string Category
            {
                get { return _Category; }
                set { _Category = value; }
            }

            public double price
            {
                get { return _price; }
                set { _price = value; }
            }

            public string title
            {
                get { return _title; }
                set { _title = value; }
            }

            public Product()
            {
                Logger.GetLog.log("Product class object created.");
                this.title = "";
                this.price = 0;
                this.Category = "";
                this.Manufacturer = "";
                this.ProductCode = "";
            }

            public Product(string title, float price, string Category, string Manufacturer, string ProductCode)
            {
                Logger.GetLog.log("Product class object created.");
                this.title = title;
                this.price = price;
                this.Category = Category;
                this.Manufacturer = Manufacturer;
                this.ProductCode = ProductCode;
            }

            public void WriteDetails()
            {
                #region Writing Object in File
                FileStream fs = new FileStream(@"C:\Users\admin\Desktop\msnet_Nazim_36456\Assignment\Assignment 3\Solution\Assignment3\Assign.txt",
                                                FileMode.OpenOrCreate,
                                                FileAccess.Write);
                Logger.GetLog.log("File is Open for Write Operation \n");
                BinaryFormatter writer = new BinaryFormatter();

                //Product p = new Product();
                Console.WriteLine("Enter Title      :");
                this.title = Console.ReadLine();
                Console.WriteLine("Enter Price      :");
                this.price = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Enter Category      :");
                this.Category = Console.ReadLine();
                Console.WriteLine("Enter Manufacturer      :");
                this.Manufacturer = Console.ReadLine();
                Console.WriteLine("Enter Product code      :");
                this.ProductCode = Console.ReadLine();



                writer.Serialize(fs, this);    //This is where we persist employe object on hard drive


                fs.Flush();
                Logger.GetLog.log("Succesfully written on File.\n");
                writer = null;
                fs.Close();
                Logger.GetLog.log("File is Closed \n");
                #endregion
            }
            #region Reading Object in File
            public void ReadDetails()
            {
                if (File.Exists(@"C:\Users\admin\Desktop\msnet_Nazim_36456\Assignment\Assignment 3\Solution\Assignment3\Assign.txt"))
                {
                    Logger.GetLog.log("File is Open for Read Operation \n");
                    FileStream fs = new FileStream(@"C:\Users\admin\Desktop\msnet_Nazim_36456\Assignment\Assignment 3\Solution\Assignment3\Assign.txt",
                                                FileMode.Open,
                                                FileAccess.Read);

                    BinaryFormatter reader = new BinaryFormatter();


                    Product someObject = (Product)reader.Deserialize(fs);
                    Console.WriteLine(someObject.title + "\n" + someObject.price + "\n" + someObject.Category + "\n" + someObject.Manufacturer + "\n" + someObject.ProductCode);


                    reader = null;
                    fs.Close();
                    Logger.GetLog.log("File is Close.\n");

                }
                else
                {
                    Console.WriteLine("File dose not exist.");
                    Logger.GetLog.log("File not found \n");

                }
            }
            #endregion

        }

        public class Logger
        {
            private Logger()
            {
                Console.WriteLine("Logger class object created.\n");
            }
            private static Logger logger = new Logger();

            public static Logger GetLog
            {
                get
                {
                    return logger;
                }
            }
            public void log(string message)
            {
                Console.WriteLine("this is log message  :" + message + " " + DateTime.Now);
                //string saveMessage = Convert.ToString( Console.ReadLine());
                FileStream fl = new FileStream(@"C:\Users\admin\Desktop\msnet_Nazim_36456\Assignment\Assignment 3\Solution\Assignment3\log.txt",
                                                 FileMode.Append,
                                                 FileAccess.Write);

                StreamWriter writer = new StreamWriter(fl);
                writer.WriteLine(message + " " + DateTime.Now + "\n");

                writer.Flush();
                fl.Flush();
                writer.Close();
                fl.Close();
                //Console.WriteLine("log close");

            }
        }
    }
}