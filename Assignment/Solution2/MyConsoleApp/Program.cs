﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasicNS;
using TemperatureNS;

namespace assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            int choic, ch, a;
            do
            {
                Console.WriteLine("Enter Your Operation Choice");
                Console.WriteLine("0: exit , 1: Calculator Calculation, 2: Temperature conversion");
                string choice1 = Console.ReadLine();
                choic = Convert.ToInt32(choice1);
                switch (choic)
                {
                    case 0:
                        break;
                    case 1:

                        do
                        {
                            BasicCalculator obj = new BasicCalculator();
                            Console.WriteLine("Enter Value of X");
                            string data1 = Console.ReadLine();
                            int i = Convert.ToInt32(data1);

                            Console.WriteLine("Enter Value of Y");
                            string data2 = Console.ReadLine();
                            int j = Convert.ToInt32(data2);

                            Console.WriteLine("Enter Your Operation Choice");
                            Console.WriteLine("0:exit, 1: Addition, 2: Subtraction, 3: Multiplication, 4: Division");
                            string choice = Console.ReadLine();
                            ch = Convert.ToInt32(choice);
                            switch (ch)
                            {
                                case 0:
                                    break;
                                case 1:

                                    int sum = obj.Add(i, j);
                                    Console.WriteLine("Sum is " + sum.ToString());
                                    break;

                                case 2:
                                    int sub = obj.Sub(i, j);
                                    Console.WriteLine("Sub is " + sub.ToString());
                                    break;

                                case 3:
                                    int mult = obj.Mult(i, j);
                                    Console.WriteLine("Mult is " + mult.ToString());
                                    break;


                                case 4:
                                    int div = obj.Div(i, j);
                                    Console.WriteLine("Div is " + div.ToString());
                                    break;

                                default:
                                    Console.WriteLine("Invalid Choice!!");
                                    break;
                            }

                        } while (ch != 0);
                        break;

                    case 2:
                        do
                        {
                            TemperatureConverter obj1 = new TemperatureConverter();
                            Console.WriteLine("Enter Your Operation Choice");
                            Console.WriteLine("0:exit, 1: Farenheit To Celcius, 2:Celcius To Farenheit");
                            string choice2 = Console.ReadLine();
                            a = Convert.ToInt32(choice2);
                            switch (a)
                            {
                                case 0:
                                    break;
                                case 1:
                                    Console.WriteLine("Enter Farenheit temp :");
                                    string temp1 = Console.ReadLine();
                                    double x = Convert.ToInt32(temp1);
                                    double Celsius = obj1.FarenheitToCelcius(x);
                                    Console.WriteLine("Celsius Temperature is " + Celsius.ToString());
                                    break;
                                case 2:
                                    Console.WriteLine("Enter Celsius temp :");
                                    string temp2 = Console.ReadLine();
                                    x = Convert.ToInt32(temp2);
                                    double Farenheit = obj1.CelciusToFarenheit(x);
                                    Console.WriteLine("Farenheit Temperature is " + Farenheit.ToString());
                                    break;
                                default:
                                    break;
                            }
                        } while (a != 0);
                        break;
                }

            } while (choic != 0);



        }
    }
}
