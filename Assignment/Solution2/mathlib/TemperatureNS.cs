﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemperatureNS
{
    public class TemperatureConverter
    {
        public double CelciusToFarenheit(double Celcius)
        {
            double Farenheit = (Celcius * 9 / 5) + 32;
            return Farenheit;
        }

        public double FarenheitToCelcius(double Farenheit)
        {

            double Celcius = (Farenheit - 32) * 5 / 9;
            return Celcius;
        }
    }
}
