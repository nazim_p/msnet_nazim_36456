﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace assignment

{
    class Program
    {
        public static Dictionary<int, Department> DeptList = new Dictionary<int, Department>();
        public static void loadDept()
        {

            string deptstring = null;
            FileStream fs = new FileStream(@"G:\msnet_nazim_36456\Assignment\Assignment 4\Solution\Assignment4\dept.csv",
                                       FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(fs);

            while ((deptstring = reader.ReadLine()) != null)
            {
                string[] deptDetails = deptstring.Split(',');
                Department dept = new Department();
                dept.DeptNo = int.Parse(deptDetails[0]);
                dept.DeptName = deptDetails[1];
                dept.Location = deptDetails[2];
                DeptList.Add(dept.DeptNo, dept);
            }
            reader = null;
            fs.Close();

        }

        public static void loademp()
        {

            string empstring = null;
            FileStream fs = new FileStream(@"G:\msnet_nazim_36456\Assignment\Assignment 4\Solution\Assignment4\emp.csv",
                                       FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(fs);
            while ((empstring = reader.ReadLine()) != null)
            {
                string[] empDetails = empstring.Split(',');
                Employee emp = new Employee();
                emp.EmpNo = int.Parse(empDetails[0]);
                emp.Name = empDetails[1];
                emp.Designation = empDetails[2];
                emp.salary = double.Parse(empDetails[3]);
                emp.commision = double.Parse(empDetails[4]);
                int dep = int.Parse(empDetails[5]);
                DeptList[dep].employee.Add(emp);
            }
            reader = null;
            fs.Close();

        }

        public static double Calcluate_Total_Salary()
        {

            double totalsalary = 0.0;
            foreach (int key in DeptList.Keys)
            {
                List<Employee> emp = DeptList[key].employee;
                foreach (Employee e in emp)
                {
                    totalsalary += e.salary + e.commision;
                }
            }
            return totalsalary;

        }

        public static List<Employee> GetAllEmployeesByDept(int DeptNo)
        {
            return DeptList[DeptNo].employee;
        }

        public static Dictionary<int, int> DeptwiseStaffCount(Dictionary<int, Department> DeptList)
        {
            Dictionary<int, int> Count = new Dictionary<int, int>();
            foreach (int key in DeptList.Keys)
            {
                Count[key] = DeptList[key].employee.Count;
            }
            return Count;
        }

        public static Dictionary<int, double> DeptwiseAvgSal(Dictionary<int, Department> DeptList)
        {
            Dictionary<int, double> avgsal = new Dictionary<int, double>();
            foreach (int key in DeptList.Keys)
            {
                List<Employee> EMP = DeptList[key].employee;
                double avg = 0.0;
                foreach (Employee e in EMP)
                {
                    avg += e.salary;
                }
                avgsal[key] = avg / DeptList[key].employee.Count;
            }
            return avgsal;
        }

        public static Dictionary<int, double> DeptwiseMinSal(Dictionary<int, Department> DeptList)
        {
            Dictionary<int, double> minsal = new Dictionary<int, double>();
            foreach (int key in DeptList.Keys)
            {
                double minsalary = Int32.MaxValue;
                List<Employee> emp = DeptList[key].employee;
                foreach (Employee e in emp)
                {
                    if (e.salary < minsalary)
                        minsalary = e.salary;
                }
                minsal[key] = minsalary;
            }
            return minsal;
        }

        public static int menulist()
        {
            int choice;
            Console.WriteLine("0. EXIT");
            Console.WriteLine("1. Calcluate_Total_Salary");
            Console.WriteLine("2. GetAllEmployeesByDept");
            Console.WriteLine("3. DeptwiseStaffCount");
            Console.WriteLine("4. DeptwiseAvgSal");
            Console.WriteLine("5. DeptwiseMinSal");
            Console.WriteLine("Enter choice : ");
            choice = Convert.ToInt32(Console.ReadLine());
            return choice;
        }
        static void Main(string[] args)
        {
            loadDept();
            loademp();
            int choice;
            while ((choice = menulist()) != 0)
            {
                switch (choice)
                {
                    case 1:
                        double Totalsal = Calcluate_Total_Salary();
                        Console.WriteLine(Totalsal.ToString());
                        break;
                    case 2:
                        Console.WriteLine("Enter the department which emps want to print: ");
                        int number = Convert.ToInt32(Console.ReadLine());
                        List<Employee> Emp = GetAllEmployeesByDept(number);
                        foreach (Employee emp in Emp)
                        {
                            Console.WriteLine(emp.ToString());
                        }
                        break;
                    case 3:
                        Dictionary<int, int> res = DeptwiseStaffCount(DeptList);
                        foreach (int key in res.Keys)
                        {
                            Console.WriteLine("the staff in Dept with deotno " + key + " is " + res[key]);
                        }
                        break;
                    case 4:
                        Dictionary<int, double> result = DeptwiseAvgSal(DeptList);
                        foreach (int key in result.Keys)
                        {
                            Console.WriteLine("the avg sal by Dept with deotno " + key + " is " + result[key]);
                        }
                        break;
                    case 5:
                        Dictionary<int, double> resu = DeptwiseMinSal(DeptList);
                        foreach (int key in resu.Keys)
                        {
                            Console.WriteLine("the min sal by Dept with deotno " + key + " is " + resu[key]);
                        }
                        break;
                }


            }
            Console.ReadLine();
        }
    }
    class Department
    {

        private int _DeptNo;
        private string _DeptName;
        private string _Location;
        private List<Employee> _employee = new List<Employee>();



        public Department()
        {
            this.DeptNo = 0;
            this.DeptName = " ";
            this.Location = " ";
        }
        public Department(int DeptNo, string DeptName, string Location)
        {
            this.DeptNo = DeptNo;
            this.DeptName = DeptName;
            this.Location = Location;
        }



        public List<Employee> employee
        {
            get { return _employee; }
            set { _employee = value; }
        }

        public string Location
        {
            get { return _Location; }
            set { _Location = value; }
        }

        public string DeptName
        {
            get { return _DeptName; }
            set { _DeptName = value; }
        }

        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }

        public override string ToString()
        {
            return string.Format("DeptNo = {0},DeptName = {1},Location = {2}",
                                   this.DeptNo, this.DeptName, this.Location);
        }
    }
    class Employee
    {

        private int _EmpNo;
        private string _Name;
        private string _Designation;
        private double _salary;
        private double _commision;


        public Employee()
        {
            this.EmpNo = 0;
            this.Name = " ";
            this.Designation = " ";
            this.salary = 0.0;
            this.commision = 0.0;
        }
        public Employee(int EmpNo, string Name, string Designation, double salary, double commision)
        {
            this.EmpNo = EmpNo;
            this.Name = Name;
            this.Designation = Designation;
            this.salary = salary;
            this.commision = commision;
        }



        public double commision
        {
            get { return _commision; }
            set { _commision = value; }
        }

        public double salary
        {
            get { return _salary; }
            set { _salary = value; }
        }

        public string Designation
        {
            get { return _Designation; }
            set { _Designation = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public int EmpNo
        {
            get { return _EmpNo; }
            set { _EmpNo = value; }
        }

        public override string ToString()
        {
            return string.Format("EmpNo = {0},Name = {1},Designation = {2},salary = {3},commision = {4}",
                     this.EmpNo, this.Name, this.Designation, this.salary, this.commision);
        }
    }
}
